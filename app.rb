require 'sinatra'
require 'json'
require 'byebug'

after do
  content_type :json
end

get '/' do
  status 400
  { :error => 'hello' }.to_json
end

post '/equipos' do
  status 201
  { :id => 1 }.to_json
end

post '/equipos/:id_equipo/jugadores' do
  status 201
  { :id => 1 }.to_json
end

get '/equipos' do
  [
    {
      "id": 2
    },
    {
      "id": 2
    }
  ].to_json
end
